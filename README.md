CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers


INTRODUCTION
------------

The A11y Dialog module implements the [A11y Dialog JavScript Library](http://edenspiekermann.github.io/a11y-dialog/).

> "Dialog windows are especially problematic for screen reader users.
> Often times the user is able to “escape” the window and interact with
> other parts of the page when they should not be able to. This is
> partially due to the way screen reader software interacts with the
> browser."
>
> "On top of that, this implementation provides all the usual mandatory
> bits from a dialog window: correct usage or ARIA landmarks, absolute
> freedom in regard to the styling, closing when pressing ESC, closing
> when clicking the background overlay, closing when clicking the close
> button, a simple yet powerful DOM API, for half a kilobyte of
> JavaScript."

Since the A11y Dialog library simply defines the required markup and a minimal
script file, the styling is left up to the themer. The library also does not
provide any additional functionality like ajax-loading. So this Drupal
implementation of the A11y Dialog library will attempt to tackle some of those
missing features.

**Features Included**:

* Configure what pages (all, non-admin, admin-only, none) that the a11y-dialog
  script is included on.

* Optionally include a small css file (~3 lines) that hides the closed dialogs.

* Optionally include a small css file (~50 lines) that provides basic dialog
  styling (overlay, centered, etc.)

* Theming functions to make it easier to generate dialog markup.

* AJAX loaded dialog content.

* Generate dialogs from inline content.

* Open dialogs from the querystring.


* For a full description of the module, visit the project page:
   https://drupal.org/project/a11ydialog

* To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/a11ydialog


Requirements
-----------

This module requires the following modules:

* [Libraries API](https://www.drupal.org/project/libraries)

* [A11y Dialog Library](https://github.com/edenspiekermann/a11y-dialog)


Installation
----------

* Install as you would normally install a contributed Drupal module. See:
  https://drupal.org/documentation/install/modules-themes/modules-7
  for further information.

* Download and extract the a11y-dialog library to the
  sites/all/libraries/a11y-dialog folder. Or use the included drush .make file.


Configuration
----------

* Configure in Administration » Configuration » User Interface » A11y Dialog:

  - Choose when to automatically load the a11y-dialog library:

    Option to include the a11y-dialog on every page, no pages, non-admin, and admin-only pages.

  - Content wrapper HTML ID

    An aria-hidden attribute is automatically added to the main content's wrapping elements when a dialog is shown. This configuration allows you to specify what that selector is.

  - Include css that initially hides dialogs:

    Includes a small css file that simply adds `a11ydialog[aria-hidden="true"] { display: none; }`

  - Include css for basic styling:

    Include a small css file that provides basic styling to make center the dialog and provide an overlay.


Troubleshooting
-----


FAQ
-----


Maintainers
------

* Craig Aschbrenner (caschbre) - https://drupal.org/u/caschbre
