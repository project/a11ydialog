/**
 * @file
 * A11y Dialog integration.
 */

/**
 * A11y Dialog Container.
 *
 * This container simply holds reference to all initialized A11yDialogs along
 * with providing some additional helper functions.
 */
var a11ydialogsContainer = (function (me, $, Drupal) {

  'use strict';

  /**
   * Dialog object/function.
   *
   * @param object settings
   *   - htmlId
   *   - title (optional)
   *   - content (optional)
   */
  var Dialog = function ( settings ) {

    // - id: the html ID for the dialog.
    // - element: DOM reference to the dialog.
    // - a11yDialog: reference to A11yDialog.
    // - initialized: boolean indicator if the Dialog has been initialized.
    // - placeholderEl: reference to DOM placeholder for moving element.
    // - swappable: boolean indicating whether an inline dialog can be swapped
    //   in and out of a11ydialog-region.
    // - dialogElements:
    //   - document: reference to the role="document" DOM element.
    //   - title: reference to the title element of the dialog.
    //   - content: reference to the a11ydialog__content DOM element.
    var data = {
      id: settings.htmlId,
      element: document.getElementById(settings.htmlId),
      a11yDialog: false,
      initialized: false,
      placeholderEl: false,
      swappable: false,
      dialogElements: {
        document: false,
        title: false,
        content: false
      }
    };

    var setDialogElements = function () {
      data.dialogElements.document = data.element.getElementsByClassName('a11ydialog__document')[0];

      data.dialogElements.title = data.element.getElementsByClassName('a11ydialog__title')[0];
      data.dialogElements.title.setAttribute('id', data.id + '__title');

      data.dialogElements.content = data.element.getElementsByClassName('a11ydialog__content')[0];

    };

    var updateTitle = function ( content ) {
      data.dialogElements.title.innerHTML = content;
    };

    var updateDocument = function ( content ) {
      data.dialogElements.document.innerHTML = content;
    };

    var updateContent = function ( content ) {
      data.dialogElements.content.innerHTML = content;
    };

    var updateElement = function ( config ) {
      var contentTarget = config.hasOwnProperty('contentTarget') ? config.contentTarget : 'content';

      if (data.dialogElements.hasOwnProperty(contentTarget)) {
        data.dialogElements[contentTarget].innerHTML = config.content;
      }
    };

    /**
     * GET content into the dialog element.
     *
     * @param object config
     * - href: The URL to GET content from.
     * - contentTarget: Option to choose the 'document' or 'content' area of the
     *   dialog. The 'document' section includes title and content. Defaults to
     *   'content'.
     */
    var get = function ( config ) {
      var contentTarget = config.hasOwnProperty('contentTarget') ? config.contentTarget : 'content';

      if (data.dialogElements.hasOwnProperty(contentTarget)) {
        var request = new XMLHttpRequest();

        request.open('GET', config.href, true);
        request.onload = function () {
					if (request.status >= 200 && request.status < 400) {
						data.dialogElements[contentTarget].innerHTML = request.responseText;
						setDialogElements();
						Drupal.attachBehaviors('#' + data.id);
					}
				};
        request.send();
      }
    };

    var swap = function () {
      if (data.placeholderEl) {
        var tempEl = document.createElement('span');

        data.placeholderEl.parentNode.insertBefore(tempEl, data.placeholderEl);
        data.element.parentNode.insertBefore(data.placeholderEl, data.element);
        tempEl.parentNode.insertBefore(data.element, tempEl);
        tempEl.parentNode.removeChild(tempEl);
      }
    };

    var clearDialog = function (target) {
      var clearTarget = data.element.getAttribute('data-a11ydialog-clear-on-close') || '';

      ckeditorOff();

      if (clearTarget.length) {
        updateElement({ contentTarget: clearTarget });
      }
      else {
        updateTitle('');
        updateContent('');
      }

    };

    var ckeditorOff = function () {
      var ckeditorInstances = data.element.querySelectorAll('textarea.ckeditor-processed');

      for (var ckeditorInstance of ckeditorInstances) {
        var id = ckeditorInstance.getAttribute('id');

        if (CKEDITOR.instances.hasOwnProperty(id)) {
          Drupal.ckeditorOff(id);
        }
      }
    };

    // @todo this isn't firing when a use-ajax link triggers the dialog and
    // populates the content. The content is loaded too late.
    var refreshHelpers = function () {
      if (data.element.getElementsByTagName('form').length) {
        data.element.classList.add('a11ydialog--has-form');
      }
      else {
        data.element.classList.remove('a11ydialog--has-form');
      }

      // If the dialog title is empty then add the element-invisible class,
      // however take into consideration the title being set invisible server
      // side.
      if (data.dialogElements.title.textContent == '') {
        data.dialogElements.title.classList.add('element-invisible');
        data.dialogElements.title.classList.add('a11ydialog__title--invisible');

        // Attempt to find any Hx tags. Grab the first one and update the dialog
        // title while leaving it hidden to allow screen readers to read it.
        var titleMatch = data.dialogElements.content.querySelectorAll('h1, h2, h3')[0];

        if (titleMatch) {
          data.dialogElements.title.textContent = titleMatch.textContent;
        }
      }
      else {
        data.dialogElements.title.classList.remove('element-invisible');
        data.dialogElements.title.classList.remove('a11ydialog__title--invisible');
      }
    };

    var onShow = function ( event ) {
      var triggerEl = event.detail;

      hideSiblings();
      swap();

      if (typeof triggerEl.hasAttribute === 'function') {
        var config = {
          contentTarget: triggerEl.hasAttribute('data-a11ydialog-content-target') ? triggerEl.getAttribute('data-a11ydialog-content-target') : 'content'
        };

        if (triggerEl.hasAttribute('data-a11ydialog-get-url')) {
          config.href = triggerEl.getAttribute('data-a11ydialog-get-url');
          get(config);
        }

        else if (triggerEl.hasAttribute('data-a11ydialog-get-once-url')) {
          config.href = triggerEl.getAttribute('data-a11ydialog-get-once-url');
          get(config);
          triggerEl.removeAttribute('data-a11ydialog-get-once-url');
        }

        else if (triggerEl.hasAttribute('data-a11ydialog-content')) {
          config.content = triggerEl.getAttribute('data-a11ydialog-content');
          updateElement(config);
        }

        if (triggerEl.hasAttribute('data-a11ydialog-title')) {
          updateTitle(triggerEl.getAttribute('data-a11ydialog-title'));
        }
      }

      refreshHelpers();

      document.body.classList.add('a11ydialog--has-open');
    };

    var onHide = function ( event ) {
      showSiblings();
      swap();
      document.body.classList.remove('a11ydialog--has-open');
      data.element.removeAttribute('a11ydialog-identifier');

      if (data.element.hasAttribute('data-a11ydialog-clear-on-close')) {
        clearDialog();
      }

      refreshHelpers();
    };

    var setupEventListeners = function () {
      data.element.addEventListener('dialog:show', function ( event ) {
        onShow(event);
      });

      data.element.addEventListener('dialog:hide', function ( event ) {
        onHide(event);
      });
    };

    var setPlaceholder = function () {
      if (data.swappable) {
        var el = data.element;

        while (el = el.parentElement) {
          if (el.hasAttribute('id') && el.getAttribute('id') == 'a11ydialog-region') {
            data.placeholderEl = false;
            break;
          }
          else if (el === document.body) {
            data.placeholderEl = document.getElementById('a11ydialog-region').appendChild(document.createElement('span'));
            data.placeholderEl.setAttribute('data-a11ydialog-swap-placeholder-id', data.id);
          }
        }
      }
    };

    /**
     * Clone Template.
     *
     * Clone the a11ydialog template and add it to the a11ydialog-region. Change
     * any selectors (id, class, etc.) based on the new dialog element id.
     */
    var cloneTemplate = function () {
      var dialogTemplate = document.getElementById('a11ydialog-template');
      var clonedElement = dialogTemplate.cloneNode(true);
      var classNamePartial = data.id.replace(/_/g, '-');

      clonedElement.setAttribute('id', data.id);
      clonedElement.classList.remove('a11ydialog--a11ydialog-template');
      clonedElement.classList.add('a11ydialog--' + classNamePartial);

      return clonedElement;
    };

    /**
     * Initialize the Dialog object.
     */
    var init = function ( settings ) {
      // No dialog element found so create a clone of the template.
      if (!data.element) {
        data.element = cloneTemplate();
        document.getElementById('a11ydialog-region').appendChild(data.element);
      }

      // Target found but it is not a dialog element.
      // Create a clone of the template and insert the target element into the
      // clone.
      else if (!data.element.classList.contains('a11ydialog')) {
        var clonedTemplate = cloneTemplate();
        data.element.setAttribute('id', data.id + '--inlined');

        if (data.element.hasAttribute('data-a11ydialog-noswap')) {
          clonedTemplate.setAttribute('data-a11ydialog-noswap', '');
        }

        data.element.parentNode.insertBefore(clonedTemplate, data.element);
        clonedTemplate.getElementsByClassName('a11ydialog__content')[0].appendChild(data.element);
        data.element = clonedTemplate;
      }

      // If the dialog contains a form then we want to disable the close on
      // overlay click.
      if (settings.hasOwnProperty('closeOnOverlay') && !settings.closeOnOverlay) {
        data.element.getElementsByClassName('a11ydialog__overlay')[0].removeAttribute('data-a11y-dialog-hide');
      }

      if (!data.a11yDialog) {
        data.a11yDialog = new A11yDialog(data.element, getMainEl());
      }

      var dialogDialogEl = data.element.getElementsByClassName('a11ydialog__dialog')[0];
      dialogDialogEl.setAttribute('aria-labelledby', data.id + '__title');

      setDialogElements();
      data.swappable = !data.element.hasAttribute('data-a11ydialog-noswap');
      setPlaceholder();

      // Add event listeners.
      setupEventListeners();

      data.element.classList.add('a11ydialog--initialized');

      data.initialized = true;
    };

    if (!data.initialized) {
      init(settings);
    }

    var addEventListener = function () {
      data.element.addEventListener.apply(data.element, arguments);
    };

    return {
      show: data.a11yDialog.show,
      hide: data.a11yDialog.hide,
      setTitle: updateTitle,
      setDocument: updateDocument,
      setContent: updateContent,
      addEventListener: addEventListener,
      refreshHelpers: refreshHelpers
    };
  };

  // initialized: boolean indicator if the container has been initialized.
  // containerEl: The reference to the a11ydialog-region DOM element.
  // mainEl: The reference to the DOM #main element.
  // dialogs: Object containing reference to all Dialog objects.
  var data = {
    initialized: false,
    containerEl: false,
    mainEl: false,
    dialogs: {},
  };

  function getMainEl() {
    return data.mainEl;
  };

  function hideSiblings() {
    var bodyChildren = document.body.childNodes;

    forEach(bodyChildren, function ( index, child ) {
      if (child.nodeType === 1 && child.getAttribute('id') !== 'a11ydialog-region') {
        child.setAttribute('aria-hidden', 'true');
      }
    });
  };

  function showSiblings() {
    var bodyChildren = document.body.childNodes;

    forEach(bodyChildren, function ( index, child ) {
      if (child.nodeType === 1 && child.getAttribute('id') !== 'a11ydialog-region') {
        child.removeAttribute('aria-hidden');
      }
    });
  };

  var createDialog = function ( settings ) {
    if (!settings.hasOwnProperty('htmlId')) {
      return false;
    }

    if (data.dialogs.hasOwnProperty(settings.htmlId)) {
      return false;
    }

    data.dialogs[settings.htmlId] = new Dialog(settings);

    return data.dialogs[settings.htmlId];
  };

  /**
   * Create Dialog objects for all dialog elements and triggers.
   */
  var createDialogs = function () {
    var dialogEls = document.querySelectorAll('.a11ydialog:not(.a11ydialog--initialized)');

    forEach(dialogEls, function ( index, dialogEl ) {
      var dialogSettings = {
        htmlId: dialogEl.getAttribute('id')
      };

      createDialog(dialogSettings);
    });

    var triggers = document.querySelectorAll('[data-a11y-dialog-show]:not(.a11ydialog--initialized)');

    forEach(triggers, function ( index, trigger ) {
      // If the trigger has the href property, change it to a hash so that a
      // page reload / redirect is not triggered.
      if (trigger.hasAttribute('href')) {
        trigger.setAttribute('href', '#');
      }

      var triggerSettings = {
        htmlId: trigger.getAttribute('data-a11y-dialog-show'),
        closeOnOverlay: trigger.hasAttribute('data-a11ydialog-overlay-close') && trigger.getAttribute('data-a11ydialog-overlay-close') == 'false' ? false : true,
      };

      createDialog(triggerSettings);
      trigger.classList.add('a11ydialog--initialized');
    });
  };

  /**
   * Custom forEach function to work around NodeLists and NodeList.forEach
   * legacy support.
   *
   * @see https://toddmotto.com/ditch-the-array-foreach-call-nodelist-hack/
   */
  var forEach = function ( array, callback, scope ) {
    for (var i = 0; i < array.length; i++) {
      callback.call(scope, i, array[i]);
    }
  };

  /**
   * Initialize the dialog container.
   */
  var init = function () {
    if (data.initialized) {
      return data.initialized;
    }

    // The a11y-dialog script assumes the main document of the page has a main
    // id. If it is not the case, you can pass the main node as second
    // argument to the A11yDialog constructor.
    // @see https://github.com/edenspiekermann/a11y-dialog/blob/2.5.7/README.md#javascript
    data.mainEl = document.getElementById(Drupal.settings.a11ydialog.mainSelector);

    if (!data.mainEl) {
      data.mainEl = document.body.getElementsByTagName('div')[0];
    }

    // Ensure we have the a11ydialog-region that contains all of the dialogs.
    // We also want this container to be the last child to the body tag so we
    // can more easily apply the aria-hidden attribute to siblings.
    data.containerEl = document.getElementById('a11ydialog-region');

    if (!data.containerEl) {
      data.containerEl = document.createElement('div');
      data.containerEl.id = 'a11ydialog-region';
    }

    document.body.appendChild(data.containerEl);
    data.initialized = true;

    return data.initialized;
  };

  window.addEventListener('load', function () {
    function getQueryParameters() {
      return document.location.search.replace(/(^\?)/,'').split("&").map(function(n){return n = n.split("="),this[n[0]] = n[1],this}.bind({}))[0];
    }

    var queryParams = getQueryParameters();

    if (queryParams.hasOwnProperty('a11ydialog')) {
      // Open existing dialog.
      if (data.dialogs.hasOwnProperty(queryParams.a11ydialog)) {
        data.dialogs[queryParams.a11ydialog].show();
      }
      // Create new dialog and open.
      else if (Drupal.settings.a11ydialog.createOnQuery) {
        var queryDialog = createDialog({
          htmlId: queryParams.a11ydialog
        });
        queryDialog.show();
      }
    }
  });

  /**
   * Initialize the container and dialogs using Drupal Behaviors. We need to
   * wait on behaviors to gain access to Drupal.settings.
   */
  Drupal.behaviors.a11ydialog = {
    attach: function ( context, settings ) {

      init();
      createDialogs();

    }
  };

  me = {
    /**
     * Create new Dialog.
     *
     * @param object settings
     * - htmlId: The html ID for the new dialog element.
     */
    createDialog: createDialog,

    getDialog: function ( dialogID ) {
      return data.dialogs.hasOwnProperty(dialogID) ? data.dialogs[dialogID] : false;
    },

    closeAll: function () {
      var dialogEls = document.querySelectorAll('.a11ydialog:not([aria-hidden="true"])');

      forEach(dialogEls, function ( index, dialogEl ) {
        var dialogID = dialogEl.getAttribute('id');
        me.getDialog(dialogID).hide();
      });
    }
  };

  return me;

}(a11ydialogsContainer || {}, jQuery, Drupal));

/**
 * Drupal Helper Behaviors / extensions.
 */
(function ($, Drupal, window, document) {

  'use strict';

  // To understand behaviors, see https://drupal.org/node/756722#behaviors

  $.fn.extend({
    // Allow the user of jquery to easily close a dialog.
    // Usage: $('#my_dialog_id').a11ydialogClose();
    a11ydialogClose: function () {
      var dialogID = $(this).attr('id');
      a11ydialogsContainer.getDialog(dialogID).hide();
    },
    // Allow the user of jquery to easily open a dialog.
    // Usage: $('#my_dialog_id').a11ydialogOpen();
    a11ydialogOpen: function () {
      // Close all dialogs first.
      a11ydialogsContainer.closeAll();

      var dialogID = $(this).attr('id');
      a11ydialogsContainer.getDialog(dialogID).show();
      $(this).a11ydialogAttachBehaviors();
    },
    // Dialog content may be populated using ajax. Allow behaviors to be
    // attached to the new content.
    a11ydialogAttachBehaviors: function () {
      var dialogID = $(this).attr('id');
      Drupal.attachBehaviors($('#' + dialogID));
    },
    a11ydialogRefreshHelpers: function () {
      var dialogID = $(this).attr('id');
      a11ydialogsContainer.getDialog(dialogID).refreshHelpers();
    }
  });

  Drupal.a11ydialog = {
    openDialog: function (ajax, response, status) {
      var dialogID = response.data.dialog_id;
      var $dialog = $('#' + dialogID);

      if (response.data.hasOwnProperty('title')) {
        $dialog
          .find('.a11ydialog__title')
          .html(response.data.title);
      }

      if (response.data.hasOwnProperty('content')) {
        $dialog
          .find('.a11ydialog__content')
          .html(response.data.content);
      }

      if (response.data.hasOwnProperty('identifier')) {
        $dialog.attr('a11ydialog-identifier', response.data.identifier);
      }

      a11ydialogsContainer.getDialog(dialogID).show();
      Drupal.attachBehaviors($('#' + dialogID));
    },

    closeDialog: function (ajax, response, status) {
      var dialogID = response.data.dialog_id;
      var $dialog = $('#' + dialogID);

      $dialog.a11ydialogClose();

      if (response.data.wipe) {
        $dialog
          .find('.a11ydialog__title')
          .html('');
        $dialog
          .find('.a11ydialog__content')
          .html('');
      }
    }
  };

  Drupal.ajax.prototype.commands.a11ydialog_openDialog = Drupal.a11ydialog.openDialog;
  Drupal.ajax.prototype.commands.a11ydialog_closeDialog = Drupal.a11ydialog.closeDialog;

  // Any a11ydialog close <button> within a form should avoid submitting the
  // form and just close the dialog.
  Drupal.behaviors.a11ydialogFormCloseButton = {
    attach: function ( context, settings) {

      $('form button[data-a11y-dialog-hide]', context)
        .once('a11ydialog-form-close-button')
        .on('click', function (e) {
          e.preventDefault();
          e.stopPropagation();
          $(this).closest('.a11ydialog').a11ydialogClose();
        });

    }
  };

  // In some cases a link <a> may be the trigger for a modal. If that link
  // contains the use-ajax class to potentially load content into the modal then
  // that link may eventually stop opening the modal. So adding a simple
  // behavior to make it open.
  Drupal.behaviors.a11ydialogUseAjaxOpen = {
    attach: function ( context, settings ) {

      $('.use-ajax.a11ydialog--initialized', context)
        .once('a11ydialog-use-ajax-open')
        .on('click', function (e) {
          var targetDialog = $(this).attr('data-a11y-dialog-show');
          $('#' + targetDialog).a11ydialogOpen();
          $('#' + targetDialog).a11ydialogRefreshHelpers();
        });

    }
  };

  // The shared dialog doesn't close automatically when clicking on the overlay.
  // This is to help when dialogs have forms that we do not want to lose by
  // accidentally clicking on the overlay. However if the dialog does not have a
  // form then we can close the dialog by clicking on the overlay.
  Drupal.behaviors.a11ydialogSharedNoform = {
    attach: function ( context, settings ) {

      $('#a11ydialog-shared')
        .once('a11ydialog-no-form-close')
        .on('click', '.a11ydialog__overlay', function (e) {
          if (!$(this).closest('#a11ydialog-shared.a11ydialog--has-form').length) {
            $('#a11ydialog-shared').a11ydialogClose();
          }
        });

    }
  };

})(jQuery, Drupal, this, this.document);
